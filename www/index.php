<?php
ini_set('display_errors', true);
ini_set('display_startup_errors', true);
ini_set('date.timezone', 'UTC');
date_default_timezone_set('UTC');

require_once __DIR__ . '/../vendor/autoload.php';

$dailyItemsStream = include '../source/itemsStream.php';

$exampleWorld = \Po\Factory\WorldFactory::createDefaultWorld();
$exampleWorld->run($dailyItemsStream);

echo sprintf('<H1>Example post office</H1>');
echo sprintf('<H2>Result: %s</H2>', $exampleWorld->getTotalDiscontentIndex());
echo sprintf('Total discontent index: %d.<br/>', $exampleWorld->getTotalDiscontentIndex());
echo sprintf('Total discontent users: %d.<br/>', $exampleWorld->getTotalDiscontentUsers());
echo sprintf('Total days: %d.<br/>', $exampleWorld->getCurrentDay());
echo sprintf('Items lost: %d.<br/>', $exampleWorld->getItemsInCount() - $exampleWorld->getItemsOutCount());


$candidateWorld = \Po\Factory\WorldFactory::createCandidateWorld();
$candidateWorld->run($dailyItemsStream);

$totalIndex = $candidateWorld->getTotalDiscontentIndex();
if($totalIndex <= 7) {
    $result = '<span style="color:#065480">Unbelievable!!!</span>';
}
elseif($totalIndex <= 10) {
    $result = '<span style="color:green">Good!</span>';
}
elseif($totalIndex <= 20) {
    $result = '<span style="color:#d4b800">Normal.</span>';
}
else {
    $result = '<span style="color:red">Bad</span>';
}
echo sprintf('<H1>Candidate post office</H1>');
echo sprintf('<h2>Status: %s</h2>', $result);
echo sprintf('<H2>Result: %s</H2>', $totalIndex);
echo sprintf('Total discontent index: %d.<br/>', $candidateWorld->getTotalDiscontentIndex());
echo sprintf('Total discontent users: %d.<br/>', $candidateWorld->getTotalDiscontentUsers());
echo sprintf('Total days: %d.<br/>', $candidateWorld->getCurrentDay());
echo sprintf('Items lost: %d.<br/>', $candidateWorld->getItemsInCount() - $candidateWorld->getItemsOutCount());