<?php
namespace Po;

use Po\Entity\Item\ItemAbstract;
use Po\Entity\Postman\PostmanAbstract;
use Po\Entity\PostOffice\PostOfficeInterface;
use Po\Factory\ItemFactory;

class World
{
    /** @var ItemFactory */
    private $itemFactory;
    /** @var PostOfficeInterface */
    private $postOffice;

    /** @var int total of overdue days */
    private $totalDiscontentIndex;
    /** @var int */
    private $totalDiscontentUser;
    /** @var int */
    private $currentDay;
    /** @var int */
    private $itemsInCount;
    /** @var int */
    private $itemsOutCount;
    /** @var array Format [numberDay][] => serialized item (type-count-lifetime, for example l-10-3 => 10 letters with lifetime = 3 days) */
    private $dailyItemsStream = [];

    public function __construct(ItemFactory $itemFactory, PostOfficeInterface $postOffice)
    {
        $this->itemFactory = $itemFactory;
        $this->postOffice = $postOffice;
    }

    public function getCurrentDay()
    {
        return $this->currentDay;
    }

    public function getTotalDiscontentIndex()
    {
        return $this->totalDiscontentIndex;
    }

    public function getTotalDiscontentUsers()
    {
        return $this->totalDiscontentUser;
    }

    public function getItemsInCount()
    {
        return $this->itemsInCount;
    }

    public function getItemsOutCount()
    {
        return $this->itemsOutCount;
    }

    /**
     * @param array
     * @return int
     */
    public function run(array $dailyItemsStream)
    {
        $this->createWorld($dailyItemsStream);
        $this->live();
    }

    private function createWorld(array $dailyItemsStream)
    {
        $this->clearWorld();
        $this->setDailyItemsStream($dailyItemsStream);
    }

    private function clearWorld()
    {
        $this->totalDiscontentIndex = 0;
        $this->totalDiscontentUser = 0;
        $this->currentDay = 0;
        $this->itemsInCount = 0;
        $this->itemsOutCount = 0;
        $this->dailyItemsStream = [];
    }

    private function setDailyItemsStream($dailyItemsStream)
    {
        $this->dailyItemsStream = $dailyItemsStream;
    }

    private function live()
    {
        while(true) {
            if($this->isAllItemsDelivered()) {
                break;
            }

            $this->liveDay();
        }

        $this->checkLostItems();
    }

    private function isAllItemsDelivered()
    {
        if(count($this->dailyItemsStream)) {
            return false;
        }

        if(!$this->postOffice->isAllItemsDelivered()) {
            return false;
        }

        return true;
    }

    private function liveDay()
    {
        $this->startNewDay();
        $dailyItems = $this->getDailyItemsStream();
        $postmen = $this->postOffice->liveDay($dailyItems);

        if($this->isLastDay()) {
            $this->deliverItemsByAllPostmen($postmen);
            return;
        }

        $this->deliverItemsByFullPostmen($postmen);
        return;
    }

    private function checkLostItems()
    {
        $lostItemsCount = $this->itemsInCount - $this->itemsOutCount;
        if($lostItemsCount > 0) {
            $this->totalDiscontentUser += $lostItemsCount;
            $this->totalDiscontentIndex += $lostItemsCount*$this->currentDay;
        }
    }

    private function startNewDay()
    {
        $this->currentDay++;
    }

    private function isLastDay()
    {
        if(count($this->dailyItemsStream)) {
            return false;
        }

        if(!$this->postOffice->isEmptyItemsQueue()) {
            return false;
        }

        return true;
    }

    /**
     * @return ItemAbstract[]
     */
    private function getDailyItemsStream()
    {
        if(!count($this->dailyItemsStream)) {
            return [];
        }

        $itemsOnCurrentDay = array_shift($this->dailyItemsStream);
        $items = $this->itemFactory->deserializeItems($this->currentDay, $itemsOnCurrentDay);
        $this->itemsInCount += count($items);

        return $items;
    }

    private function deliverItemsByFullPostmen(array $postmen)
    {
        $fullPostmen = $this->getFullPostmen($postmen);
        $this->deliverItemAndCalculateDiscontentOfUsers($fullPostmen);
    }

    private function deliverItemsByAllPostmen(array $postmen)
    {
        $this->deliverItemAndCalculateDiscontentOfUsers($postmen);
    }

    /**
     * @param PostmanAbstract[] $postmen
     * @return PostmanAbstract[]
     */
    private function getFullPostmen(array $postmen)
    {
        $fullPostmen = [];
        foreach ($postmen as $postman) {
            if($postman->isFull()) {
                $fullPostmen[] = $postman;
            }
        }

        return $fullPostmen;
    }

    /**
     * @param PostmanAbstract[] $postmen
     */
    private function deliverItemAndCalculateDiscontentOfUsers(array $postmen)
    {
        foreach($postmen as $postman) {
            $items = $postman->pullAllItems();
            foreach($items as $item) {
                $discontentIndex = $this->getCurrentDay() - $item->getExpirationDay();
                if($discontentIndex > 0) {
                    $this->totalDiscontentIndex += $discontentIndex;
                    $this->totalDiscontentUser++;
                }
            }
            $this->itemsOutCount += count($items);
        }
    }
}