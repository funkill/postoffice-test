<?php
namespace Po\Entity\PostOffice;

use Po\Entity\Item\ItemAbstract;
use Po\Entity\Postman\PostmanAbstract;
use Po\Exception\PostmanMismatchException;

class CandidatePostOffice implements PostOfficeInterface
{

    /** @var PostmanAbstract[] */
    private $postmen;
    /** @var \SplPriorityQueue */
    private $itemsQueue;
    /** @var ItemAbstract[] */
    private $deferredItems = [];

    /**
     * @param PostmanAbstract[] $postmen
     * @throws PostmanMismatchException
     */
    public function __construct(array $postmen)
    {
        $this->checkPostmen($postmen);
        $this->postmen = $postmen;
        $this->itemsQueue = new \SplPriorityQueue();
    }

    /**
     * Good time for filling postmen
     * @param ItemAbstract[] $items
     * @return PostmanAbstract[]
     */
    public function liveDay(array $items = [])
    {
        $this->checkItems($items);

        $notDeliveredItems = $this->getItemsFromPostmen();
        $items = array_merge($items, $notDeliveredItems);
        $this->pushItems($items);

        $this->fillPostmen();

        $this->pullFromDeferred();

        return $this->postmen;
    }

    /** @return bool */
    public function isAllItemsDelivered()
    {
        return $this->isEmptyItemsQueue();
    }

    /** @return bool */
    public function isEmptyItemsQueue()
    {
        return $this->itemsQueue->isEmpty();
    }

    /**
     * @param PostmanAbstract[] $postmen
     * @throws PostmanMismatchException
     */
    private function checkPostmen(array $postmen)
    {
        foreach ($postmen as $postman) {
            if (!($postman instanceof PostmanAbstract)) {
                throw new PostmanMismatchException($postman);
            }
        }
    }

    /** @param ItemAbstract[] $items */
    private function checkItems(array $items)
    {
        foreach ($items as $item) {
            $this->checkItem($item);
        }
    }

    /**
     * Check item type
     *
     * Empty realisation because php check type of argument anf throw exception if type is not equals
     *
     * @param ItemAbstract $item
     */
    private function checkItem(ItemAbstract $item)
    {
    }

    /** @return ItemAbstract[] */
    private function getItemsFromPostmen()
    {
        $items = [];
        foreach ($this->postmen as $postman) {
            if ($postman->hasItems()) {
                $items = array_merge($items, $postman->pullAllItems());
            }
        }

        return $items;
    }

    /**
     * @param ItemAbstract[] $items
     */
    private function pushItems(array $items)
    {
        foreach ($items as $item) {
            $this->checkItem($item);

            $this->itemsQueue->insert($item, $item->getExpirationDay() * -1);
        }
    }

    private function fillPostmen()
    {
        $this->itemsQueue->rewind();
        foreach ($this->itemsQueue as $item) {
            if (!$this->pushItemToPostman($item)) {
                $this->pushToDeferred($item);
            }
        }
    }

    /**
     * @param ItemAbstract $item
     * @return bool
     */
    private function pushItemToPostman(ItemAbstract $item)
    {
        $postman = $this->getPostmanForItem($item);
        if (!$postman) {
            return false;
        }

        $postman->putItem($item);

        return true;
    }

    /**
     * @param ItemAbstract $item
     * @return PostmanAbstract|bool
     */
    private function getPostmanForItem(ItemAbstract $item)
    {
        $this->postmen = $this->sortPostmen($this->postmen, $item);

        foreach ($this->postmen as $postman) {
            if ($postman->isFull()) {
                continue;
            }

            if ($postman->getItemFreeSlotCount($item)) {
                return $postman;
            }
        }

        return false;
    }

    /**
     * @param PostmanAbstract[] $postmen
     * @param ItemAbstract $item
     * @return PostmanAbstract[]
     */
    private function sortPostmen(array $postmen, ItemAbstract $item)
    {
        usort(
            $postmen,
            function (PostmanAbstract $firstPostman, PostmanAbstract $secondPostman) use ($item) {
                $capacityDiff = $this->getFreeCapacityDiff($firstPostman, $secondPostman);

                if ($capacityDiff <> 0) {
                    return $capacityDiff;
                }

                return $this->getFreeItemSlotsDiff($firstPostman, $secondPostman, $item);
            }
        );

        return $postmen;
    }

    /**
     * @param PostmanAbstract $firstPostman
     * @param PostmanAbstract $secondPostman
     * @return int
     */
    private function getFreeCapacityDiff(PostmanAbstract $firstPostman, PostmanAbstract $secondPostman)
    {
        $firstFreeCapacity = $firstPostman->getTotalFreeSlotCount();
        $secondFreeCapacity = $secondPostman->getTotalFreeSlotCount();

        return $firstFreeCapacity - $secondFreeCapacity;
    }

    /**
     * @param PostmanAbstract $firstPostman
     * @param PostmanAbstract $secondPostman
     * @param $item
     * @return int
     */
    function getFreeItemSlotsDiff(PostmanAbstract $firstPostman, PostmanAbstract $secondPostman, $item)
    {
        $firstFreeSlots = $firstPostman->getItemFreeSlotCount($item);
        $secondFreeSlots = $secondPostman->getItemFreeSlotCount($item);

        return $secondFreeSlots - $firstFreeSlots;
    }

    private function pushToDeferred(ItemAbstract $item)
    {
        $this->deferredItems[] = $item;
    }

    private function pullFromDeferred()
    {
        $this->pushItems($this->deferredItems);
        $this->deferredItems = [];
    }

}
