<?php
namespace Po\Entity\PostOffice;

use Po\Entity\Item\ItemAbstract;
use Po\Entity\Postman\PostmanAbstract;

interface PostOfficeInterface
{
    /**
     * @param PostmanAbstract[] $postmen
     */
    public function __construct(array $postmen);

    /**
     * Good time for filling postmen
     * @param ItemAbstract[] $items
     * @return PostmanAbstract[]
     */
    public function liveDay(array $items = []);

    /**
     * @return bool
     */
    public function isEmptyItemsQueue();

    /**
     * @return bool
     */
    public function isAllItemsDelivered();
}