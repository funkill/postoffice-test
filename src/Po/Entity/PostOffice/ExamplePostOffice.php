<?php
namespace Po\Entity\PostOffice;

use Po\Entity\Item\ItemAbstract;
use Po\Entity\Postman\PostmanAbstract;

class ExamplePostOffice implements PostOfficeInterface
{
    /** @var PostmanAbstract[] */
    private $postmen;
    /** @var ItemAbstract[] */
    private $itemsQueue = [];

    public function __construct(array $postmen)
    {
        $this->checkPostmen($postmen);
        $this->postmen = $postmen;
    }

    public function liveDay(array $items = [])
    {
        $this->pushItemsInQueue($items);
        $this->fillPostmen();
        return $this->postmen;
    }

    public function isEmptyItemsQueue()
    {
        return !count($this->itemsQueue);
    }

    public function isAllItemsDelivered()
    {
        if(count($this->itemsQueue)) {
            return false;
        }

        foreach($this->postmen as $postman) {
            if($postman->hasItems()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param ItemAbstract[] $items
     */
    private function pushItemsInQueue(array $items = [])
    {
        $this->checkItems($items);
        $this->itemsQueue = array_merge($this->itemsQueue, $items);
    }

    private function fillPostmen()
    {
        foreach($this->itemsQueue as $index => $item) {
            shuffle($this->postmen);
            /** @var PostmanAbstract $postman */
            $postman = current($this->postmen);

            if(!$postman->getItemFreeSlotCount($item)) {
                continue;
            }

            $postman->putItem($item);
            unset($this->itemsQueue[$index]);
        }
    }

    private function checkPostmen(array $postmen)
    {
        foreach($postmen as $postman) {
            if(!($postman instanceof PostmanAbstract)) {
                throw new \LogicException(
                    sprintf('Postman must be an instance of PostmanAbstract class. %s given.', get_class($postman))
                );
            }
        }
    }

    private function checkItems(array $items)
    {
        foreach($items as $item) {
            if(!($item instanceof ItemAbstract)) {
                throw new \LogicException(
                    sprintf('Item must be an instance of ItemAbstract class. %s given.', get_class($item))
                );
            }
        }
    }
}