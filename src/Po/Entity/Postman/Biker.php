<?php
namespace Po\Entity\Postman;

use Po\Entity\Item\Letter;
use Po\Entity\Item\Package;
use Po\Entity\Item\Wrapper;

class Biker extends PostmanAbstract
{
    const TYPE = 'Biker';

    public function __construct()
    {
        $limits = [
            Letter::TYPE_ITEM => 2,
            Wrapper::TYPE_ITEM => 3,
            Package::TYPE_ITEM => 1,
        ];
        parent::__construct(self::TYPE, $limits);
    }
}