<?php
namespace Po\Entity\Postman;

use Po\Entity\Item\Letter;
use Po\Entity\Item\Package;
use Po\Entity\Item\Wrapper;

class Postman extends PostmanAbstract
{
    const TYPE = 'Postman';

    public function __construct()
    {
        $limits = [
            Letter::TYPE_ITEM => 3,
            Wrapper::TYPE_ITEM => 1,
            Package::TYPE_ITEM => 2,
        ];
        parent::__construct(self::TYPE, $limits);
    }
}