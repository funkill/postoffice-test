<?php
namespace Po\Entity\Postman;

use Po\Entity\Item\ItemAbstract;

abstract class PostmanAbstract
{
    /** @var string */
    private $type;
    /** @var int */
    private $totalLimit;
    /** @var array typeItem => limit */
    private $typeItemLimits = [];
    /** @var ItemAbstract[] */
    private $items = [];

    public function __construct($type, array $typeItemLimits, $totalLimit = 3)
    {
        $this->type = $type;
        $this->typeItemLimits = $typeItemLimits;
        $this->totalLimit = $totalLimit;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getTotalFreeSlotCount()
    {
        return ($this->totalLimit - $this->getItemCount());
    }

    public function getItemFreeSlotCount(ItemAbstract $item)
    {
        if(!$this->getTotalFreeSlotCount()) {
            return 0;
        }

        $itemType = $item->getType();
        return ($this->getTypeItemLimit($itemType) - $this->getTypeItemCount($itemType));
    }

    public function putItem(ItemAbstract $item)
    {
        $this->checkCanPutItem($item);
        $this->items[] = $item;
    }

    public function isFull()
    {
        return !$this->getTotalFreeSlotCount();
    }

    public function hasItems()
    {
        return (bool) $this->getItemCount();
    }

    public function getItemCount()
    {
        return count($this->items);
    }

    public function pullAllItems()
    {
        $allItems = $this->items;
        $this->clearAllItems();

        return $allItems;
    }

    private function clearAllItems()
    {
        $this->items = [];
    }

    private function checkCanPutItem(ItemAbstract $item)
    {
        $this->checkItem($item);
        $this->checkTotalItemLimit();
        $this->checkTypeItemLimit($item->getType());
    }

    private function checkItem(ItemAbstract $item)
    {
        $supportedType = array_keys($this->typeItemLimits);
        if(!in_array($item->getType(), $supportedType)) {
            throw new \LogicException(sprintf(
                'Item of type "%s" not supported. Available item types: "%s".',
                $item->getType(),
                implode('", "', $supportedType)
            ));
        }
    }

    private function checkTotalItemLimit()
    {
        if($this->getItemCount() == $this->totalLimit) {
            throw new \LogicException('Postman can transfer only %s items.');
        }
    }

    private function checkTypeItemLimit($itemType)
    {
        if($this->getTypeItemCount($itemType) >= $this->getTypeItemLimit($itemType)) {
            throw new \LogicException(
                sprintf(
                    'Postman can transfer only %s item(s) of type "%s".',
                    $this->getTypeItemLimit($itemType),
                    $itemType
                )
            );
        }
    }

    private function getTypeItemLimit($itemType)
    {
        if(!isset($this->typeItemLimits[$itemType])) {
            return 0;
        }

        return $this->typeItemLimits[$itemType];
    }

    private function getItemsOfType($itemType)
    {
        $items = [];
        foreach($this->items as $item) {
            if($item->getType() == $itemType) {
                $items[] = $item;
            }
        }

        return $items;
    }

    private function getTypeItemCount($itemType)
    {
        return count($this->getItemsOfType($itemType));
    }
}