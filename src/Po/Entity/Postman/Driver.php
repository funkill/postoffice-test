<?php
namespace Po\Entity\Postman;

use Po\Entity\Item\Letter;
use Po\Entity\Item\Package;
use Po\Entity\Item\Wrapper;

class Driver extends PostmanAbstract
{
    const TYPE = 'Driver';

    public function __construct()
    {
        $limits = [
            Letter::TYPE_ITEM => 1,
            Wrapper::TYPE_ITEM => 2,
            Package::TYPE_ITEM => 3,
        ];
        parent::__construct(self::TYPE, $limits);
    }
}