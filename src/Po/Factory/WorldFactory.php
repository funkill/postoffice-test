<?php
namespace Po\Factory;

use Po\Entity\PostOffice\PostOfficeInterface;
use Po\World;

class WorldFactory
{
    public static function createDefaultWorld()
    {
        $postOffice = PostOfficeFactory::createExamplePostOffice();
        return self::createWorld($postOffice);
    }

    public static function createCandidateWorld()
    {
        $postOffice = PostOfficeFactory::createCandidatePostOffice();
        return self::createWorld($postOffice);
    }

    private static function createWorld(PostOfficeInterface $postOffice)
    {
        $itemFactory = new ItemFactory();
        return new World($itemFactory, $postOffice);
    }
}