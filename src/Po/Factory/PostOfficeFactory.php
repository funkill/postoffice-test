<?php
namespace Po\Factory;

use Po\Entity\Postman\Biker;
use Po\Entity\Postman\Driver;
use Po\Entity\Postman\Postman;
use Po\Entity\PostOffice\CandidatePostOffice;
use Po\Entity\PostOffice\ExamplePostOffice;

class PostOfficeFactory
{
    public static function createExamplePostOffice()
    {
        return new ExamplePostOffice([new Postman(), new Biker(), new Driver()]);
    }

    public static function createCandidatePostOffice()
    {
        return new CandidatePostOffice([new Postman(), new Biker(), new Driver()]);

    }
}