<?php
/**
 * Created by PhpStorm.
 * User: funkill
 * Date: 26.08.15
 * Time: 13:18
 */

namespace Po\Exception;

class PostmanMismatchException extends \LogicException
{

    /**
     * @param mixed $postman
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct($postman, $code = 0, \Exception $previous = null)
    {
        $message = sprintf('Postman must be an `PostmanAbstract` instance! He is `%s`', get_class($postman));

        parent::__construct($message, $code, $previous);
    }

}